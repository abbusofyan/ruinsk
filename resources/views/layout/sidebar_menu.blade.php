<ul class="listview flush transparent no-line image-listview mt-2">
    <li>
        <a href="index.html" class="item">
            <div class="icon-box bg-primary">
                <ion-icon name="home-outline"></ion-icon>
            </div>
            <div class="in">
                Beranda
            </div>
        </a>
    </li>
    <li>
        <a href="page-chat.html" class="item">
            <div class="icon-box bg-primary">
                <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
            </div>
            <div class="in">
                <div>Chat</div>
                <span class="badge badge-danger">5</span>
            </div>
        </a>
    </li>
    <li>
        <a href="page-about.html" class="item">
            <div class="icon-box bg-primary">
                <ion-icon name="information-outline"></ion-icon>
            </div>
            <div class="in">
                <div>Tentang</div>
            </div>
        </a>
    </li>
    <li>
        <a href="page-chat.html" class="item">
            <div class="icon-box bg-primary">
                <ion-icon name="help-outline"></ion-icon>
            </div>
            <div class="in">
                <div>FAQ</div>
            </div>
        </a>
    </li>
    <li>
        <div class="item">
            <div class="icon-box bg-primary">
                <ion-icon name="moon-outline"></ion-icon>
            </div>
            <div class="in">
                <div>Mode Gelap</div>
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input dark-mode-switch"
                        id="darkmodesidebar">
                    <label class="custom-control-label" for="darkmodesidebar"></label>
                </div>
            </div>
        </div>
    </li>
</ul>
