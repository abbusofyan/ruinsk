<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Ruinsk</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="manifest" href="__manifest.json">
    <link rel="stylesheet" href="{{asset('css/fakeLoader.css')}}">
    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">
    @yield('css')
</head>

<body>
    @include('layout.loader')
    @include('layout.header')
    <div id="appCapsule">
        @yield('content')
    </div>
    @include('layout.buttom_button')
    <div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    @include('layout.sidebar_profile')
                    @include('layout.sidebar_menu')
                </div>
                @include('layout.sidebar_button')
            </div>
        </div>
    </div>
    @include('layout.script')
    @yield('js')
</body>
</html>
