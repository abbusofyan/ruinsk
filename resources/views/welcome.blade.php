<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Ruinsk</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/img/icon/192x192.png">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="manifest" href="__manifest.json">
    <link rel="stylesheet" href="{{asset('css/fakeLoader.css')}}">
    <link rel="stylesheet" href="{{asset('css/swiper.min.css')}}">
    <style>
        .table td,
        .table th {
          border-top: none;
        }
    </style>
</head>

<body>

    @include('layout.loader')

    @include('layout.header')

    <div id="appCapsule">
        @yield('content')
    </div>

    @include('layout.buttom_button')

    <div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    @include('layout.sidebar_profile')
                    @include('layout.sidebar_menu')
                </div>
                @include('layout.sidebar_button')
            </div>
        </div>
    </div>
    @include('layout.script')
    <script type="text/javascript">
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Crash Type', '2014'],
                ['Balita Laki-laki', 97],
                ['Balita Perempuan', 68],
                ['Lansia Pria', 62],
                ['Lansia Wanita', 62],
                ['Disabilitas Fisik', 9],
                ['Disabilitas Intelektual', 4],
                ['Disabilitas Mental', 6],
                ['Disabilitas Sensorik', 3]
            ]);
            var options = {
                pieSliceText: 'Single Vehicle',
                slices: {
                    0: { color: '#f7b15b', offset: 0.2 },
                    1: { color: '#ffca71' },
                    2: { color: '#be7f29' },
                    3: { color: '#9f6407' },
                    4: { color: '#7c4600' },
                    5: { color: '#5d2d00' },
                    6: { color: '#5d2d00' },
                    7: { color: '#5d2d00' },
                    8: { color: '#5d2d00' },
                },
                backgroundColor: '#f9f9fb',
                chartArea: { left: 0, top: 20, width: '100%', height: '90%' },
            };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>
    <script>
        setTimeout(() => {
            notification('notification-welcome', 5000);
        }, 2000);

    </script>

</body>

</html>
