<div class="appBottomMenu">
    <a href="index.html" class="item active">
        <div class="col">
            <ion-icon name="home"></ion-icon>
            <p class="text-menu">Home</p>
        </div>
    </a>
    <a href="page-chat.html" class="item">
        <div class="col">
            <ion-icon name="chatbubble-ellipses-outline"></ion-icon>
            <span class="badge badge-danger">5</span>
            <p class="text-menu">Pesan</p>
        </div>
    </a>
    <a href="app-components.html" class="item">
        <div class="col call mx-auto">
            <img src="{{asset('img/phone-call.png')}}" width="40" height="40" alt="">
        </div>
    </a>
    <a href="weather-page.html" class="item" data-toggle="modal" data-target="#sidebarPanel">
        <div class="col">
            <ion-icon name="rainy-outline"></ion-icon>
            <p class="text-menu">Cuaca</p>
        </div>
    </a>
    <a href="page-register.html" class="item">
        <div class="col">
            <ion-icon name="person-outline"></ion-icon>
            <p class="text-menu">Profil</p>
        </div>
    </a>
</div>
