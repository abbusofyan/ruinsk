<script src="{{asset('js/lib/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('js/lib/popper.min.js')}}"></script>
<script src="{{asset('js/lib/bootstrap.min.js')}}"></script>
<script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
<script src="{{asset('js/plugins/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/plugins/jquery-circle-progress/circle-progress.min.js')}}"></script>
<script src="{{asset('js/base.js')}}"></script>
<script src="{{asset('js/fakeLoader.js')}}"></script>
<script src="{{asset('js/swiper.min.js')}}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
