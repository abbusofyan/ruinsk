@extends('layout.app')

@section('style')
  <style>
      .table td,
      .table th {
        border-top: none;
      }
  </style>
@endsection

@section('content')
  <div class="banner-section bg-primary">
      <div class="intro-app" style="margin-top: 10px;">
          <div class="container">
              <div class="intro-content">
                  <div class="mask"></div>
                  <img src="{{asset('img/banner/intro.jpg')}}" alt="image-demo">
                  <div class="caption">
                      <h4 class="text-white mb-1">Welcome to Ruinrisk</h4>
                      <p class="text-white">Disaster Information & Mitigation</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="container" id="cuaca">
          <div class="card shadow mt-1">
              <div class="card-body">
                  <div class="row mt-0">
                      <div class="col col-7">
                          <small class="text-dark text-cuaca">Prakiraan Cuaca Panggarangan</small>
                      </div>
                      <div class="col col-5 float-right">
                          <small class="text-dark text-cuaca">Sabtu, 28 Augustus 2021</small>
                      </div>
                  </div>

                  <div class="container">
                      <table class="table table-borderless">
                          <thead>
                              <tr class="text-center">
                                  <th scope="col"><small class="text-dark">Siang</small></th>
                                  <th scope="col"><small class="text-dark">Malam</small></th>
                                  <th scope="col"><small class="text-dark">Suhu(&#176;)</small></th>
                                  <th scope="col"><small class="text-dark">Kelembapan(%)</small></th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr class="text-center">
                                  <td scope="row"><ion-icon style="font-size: 16px;" class="text-dark" name="cloudy-night"></ion-icon></td>
                                  <td><ion-icon style="font-size: 16px;" class="text-dark" name="rainy"></ion-icon></td>
                                  <td><small class="text-dark">20-32</small></td>
                                  <td><small class="text-dark">55-90</small></td>
                              </tr>
                          </tbody>
                      </table>
                  </div>

              </div>
          </div>
      </div>
  </div>
  <div class="section" id="menu-utama">
      <div class="card shadow">
          <div class="card-body" align="center">
              <div class="menu text-center">
                  <a href="location-page.html" class="btn btn-menu py-4">
                      <img src="{{asset('img/icon-menu/map.png')}}" width="30" height="30" alt="">
                  </a>
                  <p class="text-muted">Cek Lokasi</p>
              </div>
              <div class="menu text-center ml-2">
                  <a href="#" class="btn btn-menu py-4">
                      <img src="{{asset('img/icon-menu/earthquake.png')}}" width="30" height="30" alt="">
                  </a>
                  <p class="text-muted">Potensi</p>
              </div>
              <div class="menu ml-2 text-center">
                  <a href="#" class="btn btn-menu py-4">
                      <img src="{{asset('img/icon-menu/newspaper.png')}}" width="30" height="30" alt="">
                  </a>
                  <p class="text-muted">Berita</p>
              </div>

              <div class="menu mt-1 text-center">
                  <a href="#" class="btn btn-menu py-4">
                      <img src="{{asset('img/icon-menu/reading.png')}}" width="30" height="30" alt="">
                  </a>
                  <p class="text-muted">Cerita Rakyat</p>
              </div>
              <div class="menu mt-1 ml-2 text-center">
                  <a href="#" class="btn btn-menu py-4">
                      <img src="{{asset('img/icon-menu/heart.png')}}" width="30" height="30" alt="">
                  </a>
                  <p class="text-muted">Donasi</p>
              </div>

          </div>
      </div>
  </div>
  <div class="section mt-3 mb-3">
      <div class="card shadow">
          <div class="card-body">
              <div align="center">
                  <p>Data Masyarakat Rentan</p>
                  <div id="piechart"></div>
                  <p><small>Sumber: Gugus Mitigasi Lebak Selatan, 2021</small></p>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('js')
  <script type="text/javascript">
      google.charts.load('current', { 'packages': ['corechart'] });
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
          var data = google.visualization.arrayToDataTable([
              ['Crash Type', '2014'],
              ['Balita Laki-laki', 97],
              ['Balita Perempuan', 68],
              ['Lansia Pria', 62],
              ['Lansia Wanita', 62],
              ['Disabilitas Fisik', 9],
              ['Disabilitas Intelektual', 4],
              ['Disabilitas Mental', 6],
              ['Disabilitas Sensorik', 3]
          ]);
          var options = {
              pieSliceText: 'Single Vehicle',
              slices: {
                  0: { color: '#f7b15b', offset: 0.2 },
                  1: { color: '#ffca71' },
                  2: { color: '#be7f29' },
                  3: { color: '#9f6407' },
                  4: { color: '#7c4600' },
                  5: { color: '#5d2d00' },
                  6: { color: '#5d2d00' },
                  7: { color: '#5d2d00' },
                  8: { color: '#5d2d00' },
              },
              backgroundColor: '#f9f9fb',
              chartArea: { left: 0, top: 20, width: '100%', height: '90%' },
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart'));
          chart.draw(data, options);
      }
  </script>
  <script>
      setTimeout(() => {
          notification('notification-welcome', 5000);
      }, 2000);

  </script>
@endsection
